#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include "mede_time.h"

#define SIZE 10000
#define TRUE 0
#define FALSE 1

int main(int argc, char *argv[])
{
    char palavra[SIZE];
    size_t palavra_lenght;
    char arquivo[SIZE];
    int ocorrencias = 0;
    int i, j;
    int achou;
    FILE *f;
    long fsize;
    char *string;
    size_t palavra_length;
    size_t pos_inicial_max;

    TIMER_CLEAR;
    TIMER_START;

    // Abrir o arquivo e pegar a palavra
    if (argc == 3)
    {
        f = fopen(argv[1], "r");
        strcpy(palavra, argv[2]);
    }
    else
    {
        printf("Execute o programa com o nome do arquivo e a palavra a ser procurada\n");
        exit(1);
    }

    // Verificacao do nome do arquivo
    if (f == NULL)
    {
        printf("Arquivo nao encontrado\n");
        exit(1);
    }

    // Otimização 1: carregar todo o arquivo em memória
    fseek(f, 0, SEEK_END);
    fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    string = malloc(fsize + 1);
    fread(string, 1, fsize, f);
    fclose(f);

    string[fsize] = '\0';

    // Otimização 2: retirar chamadas para função strlen (complexidade O(n) em toda chamada)
    palavra_lenght = strlen(palavra);
    palavra_length = fsize - 1;
    pos_inicial_max = palavra_length - palavra_lenght + 1;

    #pragma omp parallel for shared(string, palavra, palavra_lenght, pos_inicial_max) private(i, j, achou) reduction(+:ocorrencias) num_threads(4)
    for (i = 0; i < pos_inicial_max; i++)
    {
        achou = TRUE;
        // Comparacao da posicao da linha com a palavra
        for (j = 0; j < palavra_lenght; j++)
        {
            if (palavra[j] != string[i + j])
            {
                achou = FALSE;
                break;
            }
        }
        if (achou == TRUE)
        {
            ocorrencias++;
        }
    }

    TIMER_STOP;

    printf("=======================================\n");
    printf("Total de ocorrencias = %d\n", ocorrencias);
    printf("=======================================\n");

    printf("Tempo: %f \n", TIMER_ELAPSED);

    free(string);
    return 0;
}
